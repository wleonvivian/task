import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import personal.Application;
import personal.vivian.logic.ExecTask;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@ComponentScan(basePackages="com")
public class ExecTaskTest {
    @Autowired
    ExecTask execTask;
    @Test
    public void testExec(){
        List<String> sortstring= Lists.newArrayList();
        sortstring.add("zhu");
        sortstring.add("wang");
       // execTask.execTask(sortstring);
        execTask.generateTask(false,2,sortstring);
    }
    @Test
    public void testAExec(){
        List<String> sortstring= Lists.newArrayList();
        sortstring.add("zhu");
        sortstring.add("wang");
        // execTask.execTask(sortstring);
        execTask.generateTask(true,2,sortstring);
    }
}
