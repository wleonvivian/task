package personal.vivian.logic;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
@Slf4j
public class Executor {
    private static Logger logger = LoggerFactory.getLogger(Executor.class);
    public static class Task implements Runnable {
        public void onComplete(){

        }
        public void onError(){

        }
        public void init(){

        }
        @Override
        public void run() {
           init();
        }
    }
    public static class AsyncTask extends Task{
        private CyclicBarrier barrier;
        private AtomicInteger atomiCount;
        public AsyncTask(CyclicBarrier barrier){
            this.barrier=barrier;
        }
        public  AsyncTask(AtomicInteger atomicInteger){
            atomiCount=atomicInteger;
        }
        @Override
        public void onComplete() {

        }

        @Override
        public void onError() {
            super.onError();
        }

        @Override
        public void init() {
           System.out.print("init async");
        }

        @Override
        public void run() {
            System.out.println("aasync thread name"+Thread.currentThread().getName());
            System.out.println("aasync run");

            int i = atomiCount.decrementAndGet();
            if(i==0){
                System.out.println("aasync fiiiinish");

            }
          /*  try {
                barrier.await();
                System.out.println("aasync finish");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }*/
        }
    }
    public static class syncTask extends Task{
        private CountDownLatch countDownLatch;
        public syncTask(CountDownLatch countDownLatch){
            this.countDownLatch=countDownLatch;
        }
        @Override
        public void onComplete() {
            countDownLatch.countDown();
        }

        @Override
        public void onError() {
            super.onError();
        }

        @Override
        public void init() {
            System.out.println("sync init");
        }

        @Override
        public void run() {
            countDownLatch.countDown();
            logger.info("log sync finish");

        }
    }
    public static interface OnComplete{
        public void complete();
    }
}
