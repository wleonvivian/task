package personal.vivian.logic;

public interface Action {
    void call();
}
