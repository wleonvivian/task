package personal.vivian.logic;

public interface Function<R,T> {
    R execute(T t);
}
